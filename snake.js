
star={

  direction:39,
  tb_posi_l:[],
  tb_posi_t:[],
  tb_el:[],
  tb_grille_l:[],
  tb_grille_t:[],
  tb_rotation:[],
  taille_pixel:20,
  vitesse:80,
  bout:'',
  inter:'',
  point:0,
  
  creason:function(chemin){
      
        let audio_el=document.createElement('audio');
      
      let s_ogg=document.createElement('source');
      s_ogg.setAttribute('type','audio/ogg');
      s_ogg.setAttribute('src',chemin+'.ogg');
      audio_el.appendChild(s_ogg);
      
      let s_mp3=document.createElement('source');
      s_mp3.setAttribute('type','audio/mp3');
      s_mp3.setAttribute('src',chemin+'.mp3');
      audio_el.appendChild(s_mp3);
      return audio_el;
    },
    
  
  antibug:0
  }
  
   
  function play(idPlayer, control) {
    let player = document.querySelector('#idPlayer');

    if (player.paused) {
        player.play();
        control.textContent = 'Pause';
    } else {
        player.pause();	
        control.textContent = 'Play';
    }
}

function resume(idPlayer) {
    let player = document.querySelector('#idPlayer');

    player.currentTime = 0;
    player.pause();
}

  function star_touch(evt){	// gestion du sens de deplacement en fonction des touches clavier
  
    if(star.antibug==1){
      return false;
    }
    star.antibug=1;
    
    let touche=evt.keyCode;
    
    if(touche==39 && star.direction==37){
      return false;
    }
    if(touche==37 && star.direction==39){
      return false;
    }
    if(touche==40 && star.direction==38){
      return false;
    }
    if(touche==38 && star.direction==40){
      return false;
    }
    
    if(touche!=star.direction){
      star.direction=touche;
    }
  }
  
  function star_bouge(evt){	//deplacement de létoile et du corp.
  
    if(star.direction==39){
  
      star.tb_posi_l.unshift(star.tb_posi_l[0]+star.taille_pixel);
      star.tb_posi_l.pop();
      star.tb_posi_t.unshift(star.tb_posi_t[0]);
      star.tb_posi_t.pop();
      star.tb_rotation.unshift(0);
      star.tb_rotation.pop();
    }
    if(star.direction==37){
  
      star.tb_posi_l.unshift(star.tb_posi_l[0]-star.taille_pixel);
      star.tb_posi_l.pop();
      star.tb_posi_t.unshift(star.tb_posi_t[0]);
      star.tb_posi_t.pop();
      star.tb_rotation.unshift(180);
      star.tb_rotation.pop();
    }
    if(star.direction==38){
  
      star.tb_posi_t.unshift(star.tb_posi_t[0]-star.taille_pixel);
      star.tb_posi_t.pop();
      star.tb_posi_l.unshift(star.tb_posi_l[0]);
      star.tb_posi_l.pop();
      star.tb_rotation.unshift(270);
      star.tb_rotation.pop();
    }
    if(star.direction==40){
  
      star.tb_posi_t.unshift(star.tb_posi_t[0]+star.taille_pixel);
      star.tb_posi_t.pop();
      star.tb_posi_l.unshift(star.tb_posi_l[0]);
      star.tb_posi_l.pop();
      star.tb_rotation.unshift(90);
      star.tb_rotation.pop();
    }
    
    cadre_bord();
    
    for(let i = 0; i < star.tb_el.length; i++){
      star.tb_el[i].style.left=star.tb_posi_l[i]+'px';
      star.tb_el[i].style.top=star.tb_posi_t[i]+'px';
      rotanav(star.tb_el[i],star.tb_rotation[i]);
    }
    for(let i = 1; i < star.tb_el.length; i++){
  
      if(colision_queue(star.tb_el[i])){
        fin();
        return false;
      }
    }
    
    if(colision()){
  
      star.tb_posi_l.push(0);
      star.tb_posi_t.push(0);
      star.bout.className='star';
      star.tb_el.push(star.bout);
      star.tb_rotation.push(0);
      star.bout=crea_queue();
      star.point+=5;
      document.getElementById('point').firstChild.nodeValue=star.point;
      star.aud.currentTime=0;
      star.aud.play();
    }
    
    star.antibug=0;
  }
  
  function rotanav(el,alpha){		//rotation des div en fonction du navigateur
    
    let effet='rotate('+alpha+'deg)';
    
    el.style.transform=effet;
    
  
  
  }
  
  function cadre_bord(){		//gestion de la position aux bords du div conteneur.
  
    let conteneur=document.getElementById('dvg_jeux');
    let obj=star.tb_el[0];
    
    if(obj.offsetLeft >= conteneur.offsetWidth){
      
      star.tb_posi_l[0]=0;
    }
    
    else if(obj.offsetLeft<0){
      
      star.tb_posi_l[0]=conteneur.offsetWidth-obj.offsetWidth;
    }
    
    if(obj.offsetTop < 0){
      star.tb_posi_t[0]=conteneur.offsetHeight-obj.offsetHeight;
    }
  
    else if(obj.offsetTop >= conteneur.offsetHeight){
      star.tb_posi_t[0]=0;
    }
  }
  
  function colision(){		//gestion de la colision entre la planete et l'étoile
  
    let el=star.tb_el[0];
    let el2=star.bout;
  
    return(el.offsetTop == el2.offsetTop && el2.offsetLeft == el.offsetLeft);
  }
  
  function colision_queue(param){		//gestion de la colision entre létoile et la queue
  
    let el=star.tb_el[0];
    let el2=param;
  
    return(el.offsetTop == el2.offsetTop && el2.offsetLeft == el.offsetLeft);
  
  }
  
  function crea_queue(){	//creation des bouts de queues
  
    let el=document.createElement('div');
    el.className='pomme';
    el.style.left=star.tb_grille_l[Math.floor(Math.random()*star.tb_grille_l.length)]+ 'px';
    el.style.top= star.tb_grille_t[Math.floor(Math.random()*star.tb_grille_t.length)]+ 'px';
    
    return(document.getElementById('dvg_jeux').appendChild(el));
  }
  
   function fin(){	// fin de partie; on vide les tableaux, arrete l'interval et affiche le menu.
  
clearInterval(star.inter);
typeof window.addEventListener == 'undefined' ? document.detachEvent("onkeydown",star_touch) : removeEventListener("keydown",star_touch, false);

star.tb_posi_l.splice(0, star.tb_el.length);
star.tb_posi_t.splice(0, star.tb_el.length);
star.tb_el.splice(0, star.tb_el.length);
star.tb_grille_l.splice(0, star.tb_el.length);
star.tb_grille_t.splice(0, star.tb_el.length);
star.tb_rotation.splice(0, star.tb_el.length);
let clone=document.getElementById('menu').cloneNode(true);
let clone2=document.getElementById('dg_point').cloneNode(true);
document.getElementById('dvg_jeux').innerHTML='';
document.getElementById('dvg_jeux').appendChild(clone2);
document.getElementById('dvg_jeux').appendChild(clone);
document.getElementById('menu').style.display='block';
star.point=0;
}


  
  function init_star(){
  
    document.getElementById('menu').style.display='none';
    document.getElementById('point').firstChild.nodeValue=0;
    
    star.tb_el.push(crea_queue());
    star.tb_el[0].className='tete';
  
    star.tb_posi_l.push(star.tb_el[0].offsetLeft);
  
    star.tb_posi_t.push(star.tb_el[0].offsetTop);
    
    star.tb_rotation.push(0);
  
    for(let i=0;i < document.getElementById('dvg_jeux').offsetWidth; i+=star.taille_pixel){
      star.tb_grille_l.push(i);
    }
    
    for(let i=0;i < document.getElementById('dvg_jeux').offsetHeight; i+=star.taille_pixel){
      star.tb_grille_t.push(i);
    }
    star.bout=crea_queue();
    star.inter=setInterval(star_bouge,star.vitesse);
    star.aud=star.creason('res/son/fairy');
    typeof window.addEventListener == 'undefined' ? document.attachEvent("onkeydown",star_touch) : addEventListener("keydown",star_touch, false);
  
  }


























